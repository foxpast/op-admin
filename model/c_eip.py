# coding:utf-8
from sqlalchemy import Column, String, Integer,DateTime, and_

from database import dbconnect, session_maker, model_list, model_dict

_, _, DBase = dbconnect()


class EipInfo(DBase):
    __tablename__ = 'c_eip_info'
    __table_args__ = {'extend_existing': True}
    id = Column(Integer, primary_key=True)
    allocation_id = Column(String(255))
    ip_address = Column(String(255))
    name = Column(String(255))
    bandwidth = Column(String(255))
    isp = Column(String(255))
    instance_type = Column(String(255))
    instance_id = Column(String(255))
    department = Column(String(255))
    department_name = Column(String(255))
    resource_group = Column(String(255))
    resource_group_name = Column(String(255))
    status = Column(String(255))
    create_time = Column(String(255))
    record_time = Column(String(255))


# 通过实例id查询eip带宽信息
def find_eip_info_by_id(instance_id):
    with session_maker() as session:
        row = session.query(EipInfo).filter(and_(
            EipInfo.allocation_id == instance_id,
        )).order_by(EipInfo.record_time.desc()).first()
        return model_dict(row)
