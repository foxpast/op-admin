from sqlalchemy import Column, String, Integer, Text, and_

from database import dbconnect, session_maker, model_list, model_dict

_, md, DBase = dbconnect()


class Cluster(DBase):
    __tablename__ = 'cluster'
    __table_args__ = {'extend_existing': True}
    id = Column(Integer, primary_key=True)
    name = Column(String(255))
    apiserver = Column(String(255))
    token = Column(Text)
    prom = Column(String(255))
    skw = Column(String(255))

    def __repr__(self):
        return self.name

    # 查询所有
    def find_all(self):
        with session_maker() as session:
            result = session.query(Cluster).all()
            list = model_list(result)
            return list

    # 查询所有带有prom字段的集群
    def find_all_have_prom(self):
        with session_maker() as session:
            result = session.query(Cluster).filter(and_(Cluster.prom != None, Cluster.prom != "")).all()
            list = model_list(result)
            return list

    # 查询所有带有prom字段的集群
    def find_all_have_skw(self):
        with session_maker() as session:
            result = session.query(Cluster).filter(and_(Cluster.skw != None, Cluster.skw != "")).all()
            list = model_list(result)
            return list

    # 根据id查询
    def find_by_id(self, id):
        with session_maker() as session:
            row = session.query(Cluster).filter_by(id=id).first()
            dict = model_dict(row)
            return dict

    # 指定分页的limit和offset的参数值
    def find_limit(self, start, count):
        with session_maker() as session:
            result = session.query(Cluster).order_by(Cluster.id.asc()).limit(count).offset(start).all()
            list = model_list(result)
            return list

    def get_cluster_dict(self):
        with session_maker() as session:
            clusters = session.query(Cluster).all()
            dict = {}
            for i in clusters:
                dict[i.id] = i.name
            return dict

    def add_cluster(self, name, api_server, token, prom):
        with session_maker() as session:
            count = session.query(Cluster).filter_by(name=name).count()
            if count > 0:
                return 'already exist'
            if count == 0:
                new_cluster = Cluster(name=name, apiserver=api_server, token=token, prom=prom)
                session.add(new_cluster)
                return 'ok'


# 集群关联的 prometheus 黑盒监控web服务
class ClusterRelatedInstance(DBase):
    __tablename__ = 'cluster_related_instance'
    __table_args__ = {'extend_existing': True}
    id = Column(Integer, primary_key=True)
    cluster_id = Column(Integer)
    prom_id = Column(Integer)
    prom_url = Column(String(255))
    instance = Column(String(255))


    # 查询所有
    def find_all(self):
        with session_maker() as session:
            result = session.query(ClusterRelatedInstance).all()
            list = model_list(result)
            return list

    # 根据id查询
    def find_by_id(self, id):
        with session_maker() as session:
            row = session.query(ClusterRelatedInstance).filter_by(id=id).first()
            return model_dict(row)

    # 根据cluster_id查询
    def find_by_cluster_id(self, cluster_id):
        with session_maker() as session:
            list = session.query(ClusterRelatedInstance).filter_by(cluster_id=cluster_id).all()
            return model_list(list)

    # 添加集群关联的Instance
    def add_instance(self, cluster_id, prom_id, prom_url, instance):
        with session_maker() as session:
            new_obj = ClusterRelatedInstance(cluster_id=cluster_id, prom_id=prom_id, prom_url=prom_url,
                                                 instance=instance)
            session.add(new_obj)
            return 'ok'
