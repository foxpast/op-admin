# coding:utf-8
from sqlalchemy import Column, String, Integer, DateTime
from database import dbconnect, session_maker, model_list, model_dict

_, _, DBase = dbconnect()


class Db(DBase):
    __tablename__ = 'cmdb_db'
    __table_args__ = {'extend_existing': True}
    id = Column(Integer, primary_key=True)
    innerip = Column(String(255))
    project = Column(String(255))
    deleted = Column(Integer)
    inneraddr = Column(String(255))
    outeraddr = Column(String(255))
    owner = Column(String(255))
    name = Column(String(255))
    area = Column(String(255))
    cloud_vendor = Column(String(255))
    net_zone = Column(String(255))
    product = Column(String(255))
    env = Column(String(255))
    ins_id = Column(String(255))
    comment = Column(String(255))
    status = Column(String(255))
    db_type = Column(String(255))
    cpu = Column(String(255))
    mem = Column(String(255))
    disk = Column(Integer)
    max_conn = Column(Integer)
    max_iops = Column(Integer)
    operator = Column(String(255))
    phone = Column(String(255))
    port = Column(Integer)
    user = Column(String(255))
    password = Column(String(255))
    # 公共字段
    create_time = Column(DateTime)
    update_time = Column(DateTime)

    # 查询所有
    def find_all(self):
        with session_maker() as session:
            result = session.query(Db).filter_by(deleted=0).all()
            return model_list(result)

    # 根据id查询
    def find_by_id(self, id):
        with session_maker() as session:
            row = session.query(Db).filter_by(id=id).first()
            return model_dict(row)