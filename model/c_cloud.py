# coding:utf-8
from sqlalchemy import Column, String, Integer

from database import dbconnect, session_maker, model_list, model_dict

_, _, DBase = dbconnect()


class Cloud(DBase):
    __tablename__ = 'c_cloud'
    __table_args__ = {'extend_existing': True}
    id = Column(Integer, primary_key=True)
    cloud_name = Column(String(255))
    region_id = Column(String(255))
    root_access_key_id = Column(String(255))
    root_access_key_secret = Column(String(255))
    vendor = Column(String(255))
    type = Column(String(255))
    asapi_endpoint = Column(String(255))
    oss_endpoint = Column(String(255))
    odps_endpoint = Column(String(255))


# 查询所有
def find_all_cloud():
    with session_maker() as session:
        result = session.query(Cloud).all()
        return model_list(result)


# 查询并返回cloud_id对应的OSS endpoint
def get_oss_ep_dict():
    with session_maker() as session:
        result = session.query(Cloud).filter(Cloud.oss_endpoint.isnot(None))
        res_list = model_list(result)
        data = {}
        for c in res_list:
            data[c['id']] = c['oss_endpoint']
        return data


# 查询并返回cloud_id对应的OSS endpoint
def get_odps_ep_dict():
    with session_maker() as session:
        result = session.query(Cloud).filter(Cloud.odps_endpoint.isnot(None))
        res_list = model_list(result)
        data = {}
        for cloud in res_list:
            data[cloud['id']] = cloud['odps_endpoint']
        return data


# 查询所有专有云
def find_cloud_by_type(type):
    with session_maker() as session:
        result = session.query(Cloud).filter(Cloud.type == type).all()
        return model_list(result)


# 根据id查询
def find_cloud_by_id(id):
    with session_maker() as session:
        result = session.query(Cloud).filter(Cloud.id == id).first()
        return model_dict(result)
