# coding:utf-8
from sqlalchemy import Column, String, Integer

from database import dbconnect, session_maker, model_list, model_dict

_, _, DBase = dbconnect()


class PromSites(DBase):
    __tablename__ = 'prom_sites'
    __table_args__ = {'extend_existing': True}
    id = Column(Integer, primary_key=True)
    name = Column(String(255))
    url = Column(String(255))

    # 查询所有
    def find_all(self):
        with session_maker() as session:
            result = session.query(PromSites).all()
            list = model_list(result)
            return list

    # 根据id查询
    def find_by_id(self, id):
        with session_maker() as session:
            row = session.query(PromSites).filter_by(id=id).first()
            return model_dict(row)



