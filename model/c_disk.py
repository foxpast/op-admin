# coding:utf-8
from sqlalchemy import Column, String, Integer,DateTime, and_

from database import dbconnect, session_maker, model_list, model_dict

_, _, DBase = dbconnect()


class DiskInfo(DBase):
    __tablename__ = 'c_disk_info'
    __table_args__ = {'extend_existing': True}
    id = Column(Integer, primary_key=True)
    disk_id = Column(String(100))
    cloud_id = Column(Integer)
    category = Column(String(255))
    size = Column(String(255))
    iops = Column(String(255))
    instance_id = Column(String(255))
    department = Column(String(255))
    department_name = Column(String(255))
    resource_group = Column(String(255))
    resource_group_name = Column(String(255))
    status = Column(String(255))
    delete_with_instance = Column(String(255))
    type = Column(String(255))
    auto_snapshot_policy_id = Column(String(255))
    creation_time = Column(String(255))
    record_time = Column(DateTime)



# 通过实例id和磁盘分区名，实现模糊查找最新磁盘信息
# 当查询数据盘时，因可能存在多个，暂时只取第一条，后续优化
def find_disk_info_by_id_type(instance_id, type):
    with session_maker() as session:
        row = session.query(DiskInfo).filter(and_(
            DiskInfo.instance_id == instance_id,
            DiskInfo.type == type
        )).order_by(DiskInfo.record_time.desc()).first()
        return model_dict(row)


