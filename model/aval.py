# coding:utf-8
from sqlalchemy import Column, String, Integer, DateTime, Float

from database import dbconnect, session_maker, model_list, model_dict

_, _, DBase = dbconnect()


class Sites(DBase):
    __tablename__ = 'aval_sites'
    __table_args__ = {'extend_existing': True}
    id = Column(Integer, primary_key=True)
    name = Column(String(255))
    url = Column(String(255))
    method = Column(String(255))

    # 查询所有
    def find_all(self):
        with session_maker() as session:
            result = session.query(Sites).all()
            list = model_list(result)
            return list

    # 根据id查询
    def find_by_id(self, id):
        with session_maker() as session:
            row = session.query(Sites).filter_by(id=id).first()
            return model_dict(row)


class History(DBase):
    __tablename__ = 'aval_history'
    id = Column(Integer, primary_key=True)
    url = Column(String(255))
    up = Column(Integer)
    status_code = Column(Integer)
    duration = Column(Float)
    error = Column(String(255))
    time = Column(DateTime)