# coding:utf-8
from sqlalchemy import Column, String, Integer, DateTime

from database import dbconnect, session_maker, model_list

_, md, DBase = dbconnect()


class User(DBase):
    __tablename__ = 'users'
    __table_args__ = {'extend_existing': True}
    id = Column(Integer, primary_key=True)
    username = Column(String(255))
    password = Column(String(255))
    role = Column(String(255))
    create_time = Column(DateTime)
    last_login_time = Column(DateTime)

    # 查询所有
    def find_all(self):
        with session_maker() as session:
            result = session.query(User).all()
            return model_list(result)

    def find_by_username(self, username):
        with session_maker() as session:
            result = session.query(User).filter_by(username=username).all()
            return model_list(result)
