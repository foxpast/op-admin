# coding:utf-8
from sqlalchemy import Column, String, Integer
from database import dbconnect, session_maker, model_list, model_dict

_, md, DBase = dbconnect()


class Auditor(DBase):
    __tablename__ = 'auditor'
    __table_args__ = {'extend_existing': True}
    id = Column(Integer, primary_key=True)
    name = Column(String(50))
    phone = Column(String(50))
    comment = Column(String(50))

    # 查询所有
    def find_all(self):
        with session_maker() as session:
            result = session.query(Auditor).all()
            return model_list(result)

    # 根据id查询
    def find_by_id(self, id):
        with session_maker() as session:
            row = session.query(Auditor).filter_by(id=id).first()
            return model_dict(row)


if __name__ == '__main__':
    a = Auditor()
    li = a.find_all()
    print(li)

    from database import engine
    DBase.metadata.create_all(engine) #创建表结构