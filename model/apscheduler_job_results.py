# coding:utf-8
from sqlalchemy import Column, String, Integer, DateTime

from database import dbconnect, session_maker, model_list, model_dict

_, _, DBase = dbconnect()


class ApschedulerJobResults(DBase):
    __tablename__ = 'apscheduler_job_results'
    __table_args__ = {'extend_existing': True}
    id = Column(Integer, primary_key=True)
    job_name = Column(String(255))
    success = Column(String(255))
    message = Column(String(255))
    time = Column(DateTime)
