# coding:utf-8
from sqlalchemy import Column, String, Integer, DateTime

from database import dbconnect, session_maker, model_list

_, _, DBase = dbconnect()


class Os(DBase):
    __tablename__ = 'cmdb_os'
    __table_args__ = {'extend_existing': True}
    id = Column(Integer, primary_key=True)
    bucket = Column(String(255))
    project = Column(String(255))
    deleted = Column(Integer)
    owner = Column(String(255))
    name = Column(String(255))
    area = Column(String(255))
    cloud_vendor = Column(String(255))
    net_zone = Column(String(255))
    product = Column(String(255))
    env = Column(String(255))
    inneraddr = Column(String(255))
    outeraddr = Column(String(255))
    comment = Column(String(255))
    status = Column(String(255))
    size = Column(Integer)
    accesskey = Column(String(255))
    secret = Column(String(255))
    operator = Column(String(255))
    phone = Column(String(255))
    # 公共字段
    create_time = Column(DateTime)
    update_time = Column(DateTime)

    # 查询所有
    def find_all(self):
        with session_maker() as session:
            result = session.query(Os).filter_by(deleted=0).all()
            return model_list(result)
