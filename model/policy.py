# coding:utf-8
from sqlalchemy import Column, String, Integer

from database import dbconnect, session_maker, model_list, model_dict

_, _, DBase = dbconnect()


class Policy(DBase):
    __tablename__ = 'policy'
    __table_args__ = {'extend_existing': True}
    id = Column(Integer, primary_key=True)
    code = Column(String(255))
    project = Column(String(255))  # 新增加字段
    cluster_id = Column(Integer)
    db_host = Column(String(255))
    db_db = Column(String(255))
    db_user = Column(String(255))
    db_passwd = Column(String(255))
    db_charset = Column(String(255))

    # 查询所有
    def find_all(self):
        with session_maker() as session:
            result = session.query(Policy).all()
            list = model_list(result)
            return list

    # 根据id查询
    def find_by_id(self, id):
        with session_maker() as session:
            row = session.query(Policy).filter_by(id=id).first()
            return model_dict(row)
