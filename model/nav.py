# coding:utf-8
from sqlalchemy import Column, String, Integer
from database import dbconnect, session_maker, model_list

_, md, DBase = dbconnect()


class Nav(DBase):
    __tablename__ = 'nav'
    __table_args__ = {'extend_existing': True}
    id = Column(Integer, primary_key=True)
    type = Column(String(20))
    name = Column(String(50))
    url = Column(String(100))
    comment = Column(String(100))

    # 查询所有
    def find_all(self):
        with session_maker() as session:
            result = session.query(Nav).all()
            return model_list(result)

    def find_all_public(self):
        with session_maker() as session:
            result = session.query(Nav).filter_by(type='public').all()
            return model_list(result)

    def find_all_hangzhou(self):
        with session_maker() as session:
            result = session.query(Nav).filter_by(type='hz').all()
            return model_list(result)

    # 指定分页的limit和offset的参数值
    def find_limit(self, start, count):
        with session_maker() as session:
            result = session.query(Nav).order_by(Nav.id.asc()).limit(count).offset(start).all()
            return model_list(result)

    # 统计总数
    def get_total_count(self):
        with session_maker() as session:
            count = session.query(Nav).count()
            return count