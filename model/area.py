# coding:utf-8
from sqlalchemy import Column, String, Integer

from database import dbconnect, session_maker, model_list

_, _, DBase = dbconnect()


class Area(DBase):
    __tablename__ = 'area'
    __table_args__ = {'extend_existing': True}
    id = Column(Integer, primary_key=True)
    name = Column(String(50))

    def __repr__(self):
        return self.name

    # 查询所有告警
    def find_all(self):
        with session_maker() as session:
            result = session.query(Area).all()
            list = model_list(result)
            return list
