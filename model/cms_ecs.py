# coding:utf-8
from sqlalchemy import Column, String, Integer, DateTime, between, Date, and_

from database import dbconnect, session_maker, model_list, model_dict

_, _, DBase = dbconnect()


class CmsEcs(DBase):
    __tablename__ = 'cms_ecs'
    __table_args__ = {'extend_existing': True}
    id = Column(Integer, primary_key=True)
    instance_id = Column(String(255))
    cpu_max = Column(String(255))
    cpu_min = Column(String(255))
    cpu_avg = Column(String(255))
    mem_max = Column(String(255))
    mem_min = Column(String(255))
    mem_avg = Column(String(255))
    cloud_id = Column(Integer)
    depart_id = Column(Integer)
    depart_name = Column(String(255))
    date = Column(Date)
    record_time = Column(DateTime)


def find_period_ecs_cms(start_date, end_date) -> list:
    with session_maker() as session:
        result = session.query(CmsEcs).filter(and_(
            CmsEcs.date >= start_date,
            CmsEcs.date < end_date
        )).all()
        return model_list(result)


