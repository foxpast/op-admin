# coding:utf-8
from sqlalchemy import Column, String, Integer

from database import dbconnect, session_maker, model_list, model_dict

_, _, DBase = dbconnect()


class Settings(DBase):
    __tablename__ = 'settings'
    __table_args__ = {'extend_existing': True}
    id = Column(Integer, primary_key=True)
    key = Column(String(255))
    value = Column(String(255))

    # 查询所有
    def find_all(self):
        with session_maker() as session:
            result = session.query(Settings).all()
            list = model_list(result)
            return list

    # 根据id查询
    def get_key(self, key):
        with session_maker() as session:
            row = session.query(Settings).filter_by(key=key).first()
            return model_dict(row)



if __name__ == '__main__':
    s = Settings()
    a = s.get_key('d')
    print(a)
