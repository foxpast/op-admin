# coding:utf-8
from sqlalchemy import Column, String, Integer, DateTime

from database import dbconnect, session_maker, model_list

_, _, DBase = dbconnect()


class Code(DBase):
    __tablename__ = 'cmdb_code'
    __table_args__ = {'extend_existing': True}
    id = Column(Integer, primary_key=True)
    job = Column(String(100))
    project = Column(String(255))
    deleted = Column(Integer)
    desc = Column(String(255))
    area = Column(String(255))
    product = Column(String(255))
    type = Column(String(255))
    git = Column(String(255))
    developer = Column(String(255))
    operator = Column(String(255))
    # 公共字段
    create_time = Column(DateTime)
    update_time = Column(DateTime)

    # 查询所有
    def find_all(self):
        with session_maker() as session:
            result = session.query(Code).filter_by(deleted=0).all()
            return model_list(result)
