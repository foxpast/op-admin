# op-admin

#### Demo地址
http://119.45.8.107:32000/ （账号:fuxiao  密码:abcd@1234）

#### 介绍
OPA运维平台，集成了k8s管理、发布审批、监控查看、cmdb等功能

#### 软件功能
1.  k8s管理：资源查看、容器日志、执行命令行、YAML查看和编辑等
2.  工单审批：可创建工单更新k8s容器，钉钉通知，审批通过后自动更新
3.  告警管理：接入Prometheus或skywalking告警，存入数据库并前端展示
4.  CMDB：基于Excel的资产管理表格页面，基于多字段匹配更新，实现操作幂等性
5.  云运营报表：调用阿里云OpenAPI，获取资源信息和监控，并输出报表


#### 安装教程
1.  初始化Mysql数据库
2.  参考k8s目录，修改yaml配置并部署即可

#### 软件截图
主页
![](https://img-1257855627.cos.ap-shanghai.myqcloud.com/2023-10-26-ly1Frc.png)
工单申请：K8s服务更新
![](https://img-1257855627.cos.ap-shanghai.myqcloud.com/2023-10-26-UrYppa.png)
工单审批
![](https://img-1257855627.cos.ap-shanghai.myqcloud.com/2023-10-26-UzfVns.png)
监控告警：接入Prometheus告警
![](https://img-1257855627.cos.ap-shanghai.myqcloud.com/2023-10-26-IX24EH.png)
k8s资源管理
![](https://img-1257855627.cos.ap-shanghai.myqcloud.com/2023-10-26-BkRKEG.png)
k8s集群大盘
![](https://img-1257855627.cos.ap-shanghai.myqcloud.com/2023-10-26-akTTCW.png)
k8s节点监控
![](https://img-1257855627.cos.ap-shanghai.myqcloud.com/2023-10-26-10qF9V.png)
k8s yaml编辑
![](https://img-1257855627.cos.ap-shanghai.myqcloud.com/2023-10-26-aMmHEd.png)

k8s容器日志
![](https://img-1257855627.cos.ap-shanghai.myqcloud.com/2023-10-26-cQNdG3.png)
CMDB
![](https://img-1257855627.cos.ap-shanghai.myqcloud.com/2023-10-26-JmGZZZ.png)

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
