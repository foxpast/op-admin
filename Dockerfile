FROM python:3.10-alpine as builder
WORKDIR /app
COPY requirements.txt ./
# RUN pip install -r requirements.txt -i https://pypi.douban.com/simple/
RUN pip install -r requirements.txt -i https://mirrors.aliyun.com/pypi/simple/

FROM python:3.10-alpine
RUN apk add curl
COPY --from=builder /usr/local/lib/python3.10/site-packages /usr/local/lib/python3.10/site-packages

WORKDIR /app
ENV PYTHONUNBUFFERED=1
#ENV FLASK_APP=/app/main.py
COPY . .

RUN rm -rf /etc/localtime
RUN ln -s /usr/share/zoneinfo/Asia/Shanghai /etc/localtime  && echo "Asia/Shanghai" > /etc/timezone
ENV TZ='Asia/Shanghai'

EXPOSE 5000
CMD ["python", "main.py"]