# 初始化Flask-Admin
import flask_admin
from flask_admin.contrib.sqla import ModelView

from database import dbsession
from model.auditor import Auditor
from model.cluster import Cluster, ClusterRelatedInstance

from model.cmdb_code import Code
from model.cmdb_db import Db
from model.cmdb_host import Host
from model.cmdb_kv import Kv
from model.cmdb_lb import Loadbalancer
from model.cmdb_os import Os
from model.cmdb_svc import Svc
from model.nav import Nav
from model.prom_sites import PromSites
from model.users import User
from model.cmdb_cloud_resource import CloudResource
from model.apscheduler_job_results import ApschedulerJobResults





# 实例化admin管理后台
admin = flask_admin.Admin(name='管理后台', template_mode='bootstrap3')


# 添加链接
# admin.add_link(MenuLink(name='首页', url='/', category='Links'))


# 添加模型到admin管理后台，实现模型的编辑功能

admin.add_view(ModelView(Nav, dbsession))

# 添加用户数据管理
admin.add_view(ModelView(User, dbsession, name="用户", category="Auth"))
admin.add_view(ModelView(Auditor, dbsession, name="审批人", category="Auth"))


# 添加K8s集群管理
admin.add_view(ModelView(Cluster, dbsession, category="K8s"))
admin.add_view(ModelView(ClusterRelatedInstance, dbsession, category="K8s"))
admin.add_view(ModelView(PromSites, dbsession, category="K8s"))


# 添加CMDB管理
class CloudResourceView(ModelView):
    can_view_details = True
    column_exclude_list = ['id', 'create_time', 'update_time', ]
    column_searchable_list = ['instance_id', 'ip_private', 'ip_public']
    column_filters = ['org', 'project', 'res_type','is_deleted']


admin.add_view(ModelView(Host, dbsession, category="CMDB"))
admin.add_view(ModelView(Db, dbsession, category="CMDB"))
admin.add_view(ModelView(Loadbalancer, dbsession, category="CMDB"))
admin.add_view(ModelView(Os, dbsession, category="CMDB"))
admin.add_view(ModelView(Kv, dbsession, category="CMDB"))
admin.add_view(ModelView(Svc, dbsession, category="CMDB"))
admin.add_view(ModelView(Code, dbsession, category="CMDB"))

admin.add_view(CloudResourceView(CloudResource, dbsession, name="云资源"))



admin.add_view(ModelView(ApschedulerJobResults, dbsession, category="计划任务"))




# class JenkinsView(ModelView):
#     column_descriptions = dict(
#         area='区域 / 项目',
#         name='英文代号，用做在gitlab中生成的子目录',
#         cluster_name='集群中文名，用于前端展示选择',
#         kube_id='从jenkins中获取，是Jenkins中添加的k8s凭据的id',
#         comment='补充性文字备注'
#     )
# admin.add_view(JenkinsView(JenkinsKubeInfo, dbsession))



# admin.add_view(ModelView(Area, dbsession))

# admin.add_view(ModelView(WeeklyReport, dbsession))
# admin.add_view(ModelView(Settings, dbsession))