import os


class Config():
    # 如果是本地开发环境，就从配置文件读取配置，注意配置文件不要打包到Docker中
    # Mysql
    MYSQL_HOST = os.environ.get('MYSQL_HOST')
    MYSQL_PORT = os.environ.get('MYSQL_PORT')
    MYSQL_USER = os.environ.get('MYSQL_USER')
    MYSQL_PASS = os.environ.get('MYSQL_PASS')
    MYSQL_DB = os.environ.get('MYSQL_DB')
    MYSQL_URI = "mysql+pymysql://{}:{}@{}:{}/{}?charset=utf8".format(MYSQL_USER, MYSQL_PASS, MYSQL_HOST, MYSQL_PORT,
                                                                     MYSQL_DB)

    # LDAP
    LDAP_HOST = os.environ.get('LDAP_HOST')
    LDAP_BIND_DN = os.environ.get('LDAP_BIND_DN')
    LDAP_BIND_PASS = os.environ.get('LDAP_BIND_PASS')
    LDAP_USE_SSL = False
    if os.environ.get('LDAP_USE_SSL') == "True":
        LDAP_USE_SSL = True
    LDAP_USER_SEARCH_BASE = os.environ.get('LDAP_USER_SEARCH_BASE')  # ou=People,dc=example,dc=com
    LDAP_USER_NAME_PARAM = os.environ.get('LDAP_USER_NAME_PARAM')  # uid
    LDAP_GROUP_SEARCH_BASE = os.environ.get('LDAP_GROUP_SEARCH_BASE')  # ou=Group,dc=yunyang,dc=com,dc=cn
    LDAP_GROUP_CN = os.environ.get('LDAP_GROUP_CN')  # opa-user

    # 项目初始化-jenkins
    JENKINS_URL = os.environ.get('JENKINS_URL')
    JENKINS_USER = os.environ.get('JENKINS_USER')
    JENKINS_PASSWD = os.environ.get('JENKINS_PASSWD')

    # 项目初始化-gitlab
    GITLAB_URL = os.environ.get('GITLAB_URL')
    GITLAB_TOKEN = os.environ.get('GITLAB_TOKEN')
    GITLAB_JENKINS_PROJECT_ID = os.environ.get('GITLAB_JENKINS_PROJECT_ID')
    GITLAB_JENKINS_PROJECT_BRANCH = os.environ.get('GITLAB_JENKINS_PROJECT_BRANCH')
    GITLAB_PROXY = os.environ.get('GITLAB_PROXY')

    # 工单审批
    WEBHOOK = os.environ.get('WEBHOOK')

    # api测试
    API_TEST_ADDR = os.environ.get('API_TEST_ADDR')


# K8s集群相关
K8s_API_PREFIX = {
    "pods": "/api/v1",
    "deployments": "/apis/apps/v1",
    "statefulsets": "/apis/apps/v1",
    "daemonsets": "/apis/apps/v1",
    "services": "/api/v1",
    "configmaps": "/api/v1",
    "persistentvolumes": "/api/v1",
    "persistentvolumeclaims": "/api/v1",
}
K8s_API_NAMESPACED = {
    "pods": True,
    "deployments": True,
    "statefulsets": True,
    "daemonsets": True,
    "services": True,
    "configmaps": True,
    "persistentvolumes": False,
    "persistentvolumeclaims": True,
}

UA = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36"

# 工单相关
JOB_STATE = {
    1: '待审批',
    2: '已通过',
    3: '已拒绝',
}
JOB_TYPE = {
    1: '服务更新',
    2: '新建服务',
    3: '删除服务',
}

# 用户角色
ROLE_ADMIN = ['admin', 'superadmin']
