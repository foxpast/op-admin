import json
import math

from flask import Blueprint, render_template, jsonify, request, session
from model.nav import Nav
from model.job import Job
from model.alerts import Alerts
from model.cmdb_cloud_resource import CloudResource
from model.cluster import Cluster

from blues.nav import bp


@bp.route('/')
@bp.route('/nav')
def home():
    model_nav = Nav()
    model_job = Job()
    model_alert = Alerts()
    model_cloud_resource = CloudResource()
    model_cluster = Cluster()

    # 准备主页数据
    data = {}
    data['nav_links'] = model_nav.find_all()
    data['jobs_undo'] = len(model_job.find_all_undo())
    data['alerts_firing'] = len(model_alert.find_all_by_status("firing"))
    data['cloud_resource_all'] = len(model_cloud_resource.find_all())
    data['cluster_all'] = len(model_cluster.find_all())
    return render_template('nav.html', data=data, active="nav", )


@bp.route('/nav/page/<int:page>')
def paginate(page):
    start = (page - 1) * 10
    n = Nav()
    result = n.find_limit(start, 10)
    total = math.ceil(n.get_total_count() / 10)
    return render_template('nav.html', result=result, total=total, page=page, active="nav")

