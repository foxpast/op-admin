import json
import requests

from config import Config

conf = Config()

headers = {'Content-Type': 'application/json;charset=utf-8'}


def job_create_msg(content, phone):
    json_text = {
        "msgtype": "text",
        "text": {
            "content": content
        },
        "at": {
            "atMobiles": [
                phone
            ],
            "isAtAll": False
        }
    }
    resp = requests.post(conf.WEBHOOK, json.dumps(json_text), headers=headers).content
    print(resp)
    # if WEBHOOK2 != WEBHOOK:
    #     resp2 = requests.post(WEBHOOK2, json.dumps(json_text), headers=headers).content
    #     print(resp2)


def job_succeed_msg(content):
    json_text = {
        "msgtype": "text",
        "text": {
            "content": content
        },
        "at": {
            "atMobiles": [
            ],
            "isAtAll": True
        }
    }
    resp = requests.post(conf.WEBHOOK, json.dumps(json_text), headers=headers).content
    print(resp)


if __name__ == '__main__':
    json_text = {
        "msgtype": "text",
        "text": {
            "content": """- k8s服务发布完成，请验收
工单ID：20220314055233
审批人：王方毅

集群：杭州-亲清在线政策-prod
1. py-330110-009-ent-backend-prod => 1.1.4
2. py-330110-009-gov-backend-prod => 1.1.3
3. b-service-330103-010-policy-prod => 1.2.2-prod
4. py-330103-010-ent-backend-prod => 1.0.10
5. py-330103-010-gov-backend-prod => 1.0.6
"""
        },
        "at": {
            "atMobiles": [
            ],
            "isAtAll": True
        }
    }
    resp = requests.post(conf.WEBHOOK, json.dumps(json_text), headers=headers).content
    print(resp)
