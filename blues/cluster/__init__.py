from flask import Blueprint

# 创建一个蓝图
bp = Blueprint("cluster_bp", __name__, template_folder="templates")

# 在__init__被执行的时候，把视图加载进来，让蓝图与应用程序知道有视图的存在
# 注意，必须先实例化bp，然后import其他。否则会造成循环import
from blues.cluster import cluster_base
from blues.cluster import cluster_deploy
from blues.cluster import cluster_ingress
from blues.cluster import cluster_pods
from blues.cluster import cluster_prom
from blues.cluster import cluster_proxy
from blues.cluster import cluster_views
