from flask import Blueprint, render_template

from blues.cluster import bp


@bp.route('/proxy')
def home():
    return render_template('proxy-cluster.html', active="cluster_ingresses")


@bp.route('/proxy/cluster/<id>')
def list(id):
    # print(id)
    return render_template('proxy.html', active="cluster_ingresses")


@bp.route('/proxy/cluster/<id>/add')
def add(id):
    # print(id)
    return render_template('proxy_add.html', active="cluster_ingresses")
