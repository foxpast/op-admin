from flask import render_template,request

from model.cluster import Cluster
from blues.cluster import bp


# Views
@bp.route('/cluster/')
@bp.route('/cluster/<cluster_id>')
def ViewClusters(cluster_id=0):
    return render_template('dashboard.html', active='cluster_dashboard')


@bp.route('/cluster/node_monitor')
def ViewClusterMonitor():
    return render_template('monitor_node.html', active='cluster_dashboard')


@bp.route('/cluster/pods/')
def ViewPod():
    return render_template('pods.html', active='cluster_pods')


@bp.route('/cluster/deployments/')
def ViewDeploy():
    return render_template('deployments.html', active='cluster_deployments')


@bp.route('/cluster/statefulsets/')
def ViewSTS():
    return render_template('statefulsets.html', active='cluster_statefulsets')


@bp.route('/cluster/daemonsets/')
def ViewDS():
    return render_template('daemonsets.html', active='cluster_daemonsets')


@bp.route('/cluster/services/')
def ViewSvc():
    return render_template('services.html', active='cluster_services')


@bp.route('/cluster/configmaps/')
def ViewCm():
    return render_template('configmaps.html', active='cluster_configmaps')


@bp.route('/cluster/persistentvolumes/')
def ViewPV():
    return render_template('pv.html', active='cluster_pv')


@bp.route('/cluster/persistentvolumeclaims/')
def ViewPVC():
    return render_template('pvc.html', active='cluster_pvc')


# YAML views
@bp.route('/cluster/createfromyaml')
def CreateYaml():
    return render_template('yaml-create.html', active='cluster_dashboard')


@bp.route('/cluster/<cluster_id>/<namespace>/<type>/<name>/yaml/')
def EditYaml(cluster_id, namespace, type, name):
    c = Cluster()
    cluster = c.find_by_id(cluster_id)
    return render_template('yaml.html', cluster_name=cluster['name'], active='cluster')


@bp.route('/cluster/logs/<cluster_id>/<namespace>/<pod>/<container>')
def viewLog(cluster_id, namespace, pod, container):
    c = Cluster()
    cluster = c.find_by_id(cluster_id)
    return render_template('logs.html', host=cluster['apiserver'], token=cluster['token'], namespace=namespace,
                           pod=pod, container=container, active='cluster')


@bp.route('/cluster/terminal/<cluster_id>/<namespace>/<pod>/<container>')
def execCmd(cluster_id, namespace, pod, container):
    c = Cluster()
    cluster = c.find_by_id(cluster_id)
    return render_template('terminal.html', cluster_name=cluster['name'], active='cluster')


@bp.route('/cluster/<cluster_id>/<namespace>/<type>/<name>/pods/')
def ViewWorkloadsPods(cluster_id, namespace, type, name):
    selector = request.args.get('selector')
    c = Cluster()
    cluster = c.find_by_id(cluster_id)
    if type == 'deployments':
        return render_template('deploy-pods.html', cluster_name=cluster['name'], namespace=namespace,
                               selector=selector, name=name, active='cluster')
    else:
        return render_template('pods.html', cluster_name=cluster['name'], namespace=namespace,
                               selector=selector,
                               active='cluster')


@bp.route('/cluster/<cluster_id>/<namespace>/containers/<pod>')
def cluster_containers(cluster_id, namespace, pod):
    c = Cluster()
    cluster = c.find_by_id(cluster_id)
    return render_template('containers.html', cluster_name=cluster['name'], namespace=namespace, pod=pod,
                           active='cluster')