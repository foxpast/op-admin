from flask import Blueprint

# 创建一个蓝图
bp = Blueprint("login_bp", __name__, template_folder="templates")


# 在__init__被执行的时候，把视图加载进来，让蓝图与应用程序知道有视图的存在
# 注意，必须先实例化bp，然后import其他。否则会造成循环import
from blues.login import views

