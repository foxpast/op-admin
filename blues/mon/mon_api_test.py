import requests
from flask import abort

from blues.mon import bp
from config import Config

conf = Config()


@bp.route('/api/v1/mon/api_test', methods=['GET'], endpoint="api_test")
def get_api_test_data():
    api_test_addr = conf.API_TEST_ADDR
    print(api_test_addr)
    if not api_test_addr:
        print("数据库settings表中不存在api_test_addr键")
        abort(500)

    r = requests.get(api_test_addr + "/status")
    r.encoding = r.apparent_encoding
    print(r.text)
    try:
        return r.json()
    except Exception as e:
        print(e)
        return str(e)


@bp.route('/api/v1/mon/api_test/time', methods=['GET'])
def get_api_test_time():
    api_test_addr = conf.API_TEST_ADDR
    print(api_test_addr)
    if not api_test_addr:
        print("数据库settings表中不存在api_test_addr键")
        abort(500)
    r = requests.get(api_test_addr + "/time")
    print(r.text)
    try:
        return r.json()
    except Exception as e:
        print(e)
        return str(e)
