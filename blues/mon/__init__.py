from flask import Blueprint

bp = Blueprint("mon_bp", __name__, template_folder='templates')

from blues.mon import mon_api_test
from blues.mon import mon_aval
from blues.mon import mon_views
from blues.mon import mon_weekly_report
