from flask import render_template

from blues.mon import bp


@bp.route('/mon/weekly')
def mon_weekly_view():
    return render_template('mon_weekly.html', active='mon_weekly')


@bp.route('/mon/weekly/service')
def service_view():
    return render_template('mon_weekly_service.html', active='mon_weekly_service')


@bp.route('/mon/aval')
def mon_aval_view():
    return render_template('mon_aval.html', active='mon_aval')


@bp.route('/aval/monitor/<id>')
def monitor(id):
    return render_template('mon_aval_monitor.html', active='mon_aval')


@bp.route('/mon/api_test')
def home():
    return render_template('mon_api_test.html', active='mon_api_test')
