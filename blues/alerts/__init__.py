from flask import Blueprint

bp = Blueprint("alerts_bp", __name__, template_folder='templates')

# 为了避免循环import，必须在bp实例化之后，再import其他的
from blues.alerts import views
