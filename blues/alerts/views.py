from flask import Blueprint, render_template, jsonify

from model.alerts import Alerts, AlertsSwk

from blues.alerts import bp


@bp.route('/alarm/prom/')
def web_alarm_prom():
    return render_template('alerts_prom.html', active='alarm_prom')


@bp.route('/alarm/skw/')
def web_alarm_skw():
    return render_template('alerts_skw.html', active='alarm_skw')


@bp.route('/api/v1/alarm/prom')
def api_alarm_prom():
    t = Alerts()
    result = t.find_all()
    status_dict = {"resolved": "已解决", "firing": "告警中"}
    severity_dict = {"critical": "严重", "warning": "一般", "info": "通知"}
    for i in result:
        i['status'] = status_dict[i['status']]
        if i['severity'] in severity_dict.keys():
            i['severity'] = severity_dict[i['severity']]
        else:
            i['severity'] = "无"
        # print(type(i['startsAt']))
        i['startsAt'] = i['startsAt'].strftime('%Y/%m/%d %H:%M:%S') if i['startsAt'] else ''
        i['endsAt'] = i['endsAt'].strftime('%Y/%m/%d %H:%M:%S') if i['endsAt'] else ''
    return jsonify({"code": 200, "data": result})


@bp.route('/api/v1/alarm/skw')
def api_alarm_skw():
    t = AlertsSwk()
    result = t.find_all()
    for i in result:
        # print(i)
        if i['startTime']:
            i['startTime'] = i['startTime'].strftime('%Y/%m/%d %H:%M:%S')
    return jsonify({"code": 200, "data": result})
