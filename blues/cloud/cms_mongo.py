# coding=utf-8
import json
from datetime import datetime

from flask import jsonify

from blues.cloud import bp
from blues.cloud.base import request_asapi
from database2 import db_exec_many
from model.c_department import find_all_depart
from public.time_utils import get_last_2days_0hour


def record_cms_mongo() -> bool:
    # 初始化一个空字典，稍后遍历depart并调用asapi后，将cpu,mem,disk,con,iops监控数据填充到res中
    res = {}
    yesterday_0hour, today_0hour = get_last_2days_0hour()

    # 对于专有云，只能通过部门ak sk获取cms信息
    depart_list = find_all_depart()
    for depart in depart_list:
        cloud_id = depart['cloud_id']
        depart_ak = depart['access_key_id']
        depart_sk = depart['access_key_secret']
        depart_id = depart['department']
        depart_name = depart['department_name']
        print(cloud_id, depart_id, depart_name)

        # 获取ASAPI接口数据
        params = {"Namespace": "acs_mongodb", "Period": "900", "StartTime": yesterday_0hour,
                  "EndTime": today_0hour, 'MetricName': "CPUUtilization"}

        # 获取cpu使用率
        asapi_data_cpu = request_asapi(cloud_id, "Cms", "2019-01-01", "DescribeMetricList", params, ak=depart_ak,
                                       sk=depart_sk)
        if not asapi_data_cpu:
            return False
        data_points_cpu = json.loads(asapi_data_cpu['Datapoints'])

        # 获取mem使用率
        params['MetricName'] = 'MemoryUtilization'
        asapi_data_mem = request_asapi(cloud_id, "Cms", "2019-01-01", "DescribeMetricList", params, ak=depart_ak,
                                       sk=depart_sk)
        if not asapi_data_mem:
            return False
        data_points_mem = json.loads(asapi_data_mem['Datapoints'])

        # 获取connection使用率
        params['MetricName'] = 'ConnectionUtilization'
        asapi_data_con = request_asapi(cloud_id, "Cms", "2019-01-01", "DescribeMetricList", params, ak=depart_ak,
                                       sk=depart_sk)
        if not asapi_data_con:
            return False
        data_points_con = json.loads(asapi_data_con['Datapoints'])

        # 获取iops使用率
        params['MetricName'] = 'IOPSUtilization'
        asapi_data_iops = request_asapi(cloud_id, "Cms", "2019-01-01", "DescribeMetricList", params, ak=depart_ak,
                                        sk=depart_sk)
        if not asapi_data_iops:
            return False
        data_points_iops = json.loads(asapi_data_iops['Datapoints'])

        # 获取disk使用率
        params['MetricName'] = 'DiskUtilization'
        params['Period'] = '86400'
        asapi_data_disk = request_asapi(cloud_id, "Cms", "2019-01-01", "DescribeMetricLast", params, ak=depart_ak,
                                        sk=depart_sk)
        if not asapi_data_disk:
            return False
        data_points_disk = json.loads(asapi_data_disk['Datapoints'])

        # 将上面所有的监控信息填充到字典res中
        for i in data_points_cpu:
            ins_id = i['instanceId']
            cpu_max = i['Maximum']
            cpu_min = i['Minimum']
            cpu_avg = i['Average']
            if i['instanceId'] not in res.keys():
                res[ins_id] = {
                    "cloud_id": cloud_id, "depart_id": depart_id, "depart_name": depart_name,
                    "cpu_max": [cpu_max], "cpu_min": [cpu_min], "cpu_avg": [cpu_avg],
                    "mem_max": [], "mem_min": [], "mem_avg": [],
                    "con_max": [], "con_min": [], "con_avg": [],
                    "iops_max": [], "iops_min": [], "iops_avg": [],
                    "disk_max": []
                }
                continue
            res[ins_id]["cpu_max"].append(cpu_max)
            res[ins_id]["cpu_min"].append(cpu_min)
            res[ins_id]["cpu_avg"].append(cpu_avg)

        for i in data_points_mem:
            ins_id = i['instanceId']
            mem_max = i['Maximum']
            mem_min = i['Minimum']
            mem_avg = i['Average']
            if i['instanceId'] not in res.keys():
                res[ins_id] = {
                    "cloud_id": cloud_id, "depart_id": depart_id, "depart_name": depart_name,
                    "cpu_max": [], "cpu_min": [], "cpu_avg": [],
                    "mem_max": [mem_max], "mem_min": [mem_min], "mem_avg": [mem_avg],
                    "con_max": [], "con_min": [], "con_avg": [],
                    "iops_max": [], "iops_min": [], "iops_avg": [],
                    "disk_max": []
                }
                continue
            res[ins_id]["mem_max"].append(mem_max)
            res[ins_id]["mem_min"].append(mem_min)
            res[ins_id]["mem_avg"].append(mem_avg)

        for i in data_points_con:
            ins_id = i['instanceId']
            con_max = i['Maximum']
            con_min = i['Minimum']
            con_avg = i['Average']
            if i['instanceId'] not in res.keys():
                res[ins_id] = {
                    "cloud_id": cloud_id, "depart_id": depart_id, "depart_name": depart_name,
                    "cpu_max": [], "cpu_min": [], "cpu_avg": [],
                    "mem_max": [], "mem_min": [], "mem_avg": [],
                    "con_max": [con_max], "con_min": [con_min], "con_avg": [con_avg],
                    "iops_max": [], "iops_min": [], "iops_avg": [],
                    "disk_max": []
                }
                continue
            res[ins_id]["con_max"].append(con_max)
            res[ins_id]["con_min"].append(con_min)
            res[ins_id]["con_avg"].append(con_avg)

        for i in data_points_iops:
            ins_id = i['instanceId']
            iops_max = i['Maximum']
            iops_min = i['Minimum']
            iops_avg = i['Average']
            if i['instanceId'] not in res.keys():
                res[ins_id] = {
                    "cloud_id": cloud_id, "depart_id": depart_id, "depart_name": depart_name,
                    "cpu_max": [], "cpu_min": [], "cpu_avg": [],
                    "mem_max": [], "mem_min": [], "mem_avg": [],
                    "con_max": [], "con_min": [], "con_avg": [],
                    "iops_max": [iops_max], "iops_min": [iops_min], "iops_avg": [iops_avg],
                    "disk_max": []
                }
                continue
            res[ins_id]["iops_max"].append(iops_max)
            res[ins_id]["iops_min"].append(iops_min)
            res[ins_id]["iops_avg"].append(iops_avg)

        for i in data_points_disk:
            ins_id = i['instanceId']
            disk_max = i['Maximum']
            if i['instanceId'] not in res.keys():
                res[ins_id] = {
                    "cloud_id": cloud_id, "depart_id": depart_id, "depart_name": depart_name,
                    "cpu_max": [], "cpu_min": [], "cpu_avg": [],
                    "mem_max": [], "mem_min": [], "mem_avg": [],
                    "con_max": [], "con_min": [], "con_avg": [],
                    "iops_max": [], "iops_min": [], "iops_avg": [],
                    "disk_max": [disk_max]
                }
                continue
            res[ins_id]["disk_max"].append(disk_max)

    # 将规整到字典里的数据，求max min avg
    for ins_id in res.keys():
        # 计算cpu
        res[ins_id]["cpu_max"] = max(res[ins_id]["cpu_max"]) if res[ins_id]["cpu_max"] else 0
        res[ins_id]["cpu_min"] = min(res[ins_id]["cpu_min"]) if res[ins_id]["cpu_min"] else 0
        if res[ins_id]["cpu_avg"]:
            res[ins_id]["cpu_avg"] = round(
                sum(res[ins_id]["cpu_avg"]) / len(res[ins_id]["cpu_avg"]), 2
            )
        else:
            res[ins_id]["cpu_avg"] = 0
        # 计算mem
        res[ins_id]["mem_max"] = max(res[ins_id]["mem_max"]) if res[ins_id]["mem_max"] else 0
        res[ins_id]["mem_min"] = min(res[ins_id]["mem_min"]) if res[ins_id]["mem_min"] else 0
        if res[ins_id]["mem_avg"]:
            res[ins_id]["mem_avg"] = round(
                sum(res[ins_id]["mem_avg"]) / len(res[ins_id]["mem_avg"]), 2
            )
        else:
            res[ins_id]["mem_avg"] = 0
        # 计算connection
        res[ins_id]["con_max"] = max(res[ins_id]["con_max"]) if res[ins_id]["con_max"] else 0
        res[ins_id]["con_min"] = min(res[ins_id]["con_min"]) if res[ins_id]["con_min"] else 0
        if res[ins_id]["con_avg"]:
            res[ins_id]["con_avg"] = round(
                sum(res[ins_id]["con_avg"]) / len(res[ins_id]["con_avg"]), 2
            )
        else:
            res[ins_id]["con_avg"] = 0
        # 计算iops
        res[ins_id]["iops_max"] = max(res[ins_id]["iops_max"]) if res[ins_id]["iops_max"] else 0
        res[ins_id]["iops_min"] = min(res[ins_id]["iops_min"]) if res[ins_id]["iops_min"] else 0
        if res[ins_id]["iops_avg"]:
            res[ins_id]["iops_avg"] = round(
                sum(res[ins_id]["iops_avg"]) / len(res[ins_id]["iops_avg"]), 2
            )
        else:
            res[ins_id]["iops_avg"] = 0
        # 计算disk
        res[ins_id]["disk_max"] = max(res[ins_id]["disk_max"]) if res[ins_id]["disk_max"] else 0

    # 删除res字典中获取的 "quota_for_get_service_" bucket
    res.pop("", None)
    # 打印字典内容，供检查
    for k, v in res.items():
        print(k, v)
    print("采集数据数量：", len(res))

    # 准备批量插入数据库，先初始化一个空数组，在循环中不断追加值的元组
    values_list = []
    for k, v in res.items():
        tmp_tuple = (
            k, v['cpu_max'], v['cpu_min'], v['cpu_avg'], v['mem_max'], v['mem_min'], v['mem_avg'],
            v['con_max'], v['con_min'], v['con_avg'], v['iops_max'], v['iops_min'], v['iops_avg'], v['disk_max'],
            v['cloud_id'], v['depart_id'], v['depart_name'], yesterday_0hour.rstrip(" 00:00:00"), datetime.now())
        values_list.append(tmp_tuple)
    table = 'cms_mongo'
    fields = "instance_id,cpu_max,cpu_min,cpu_avg,mem_max,mem_min,mem_avg,con_max,con_min,con_avg,iops_max,iops_min,iops_avg," \
             "disk_max,cloud_id,depart_id,depart_name,`date`,record_time"
    # 使用上面的数据，批量插入db
    db_success = db_exec_many(table, fields, values_list)
    if not db_success:
        return False

    # 整体无异常，返回正常
    return True


@bp.route('/api/v1/cloud/record_cms_mongo', methods=['POST'])
def record_cms_mongo_api():
    success = record_cms_mongo()
    if not success:
        return jsonify({'code': 500})
    return jsonify({'code': 200})
