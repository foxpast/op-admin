# coding=utf-8

from flask import jsonify, request

from blues.cloud import bp
from database2 import db_exec_one
from model.c_ecs_info import get_period_ecs_info
from model.cms_ecs import find_period_ecs_cms
from model.cms_ecs_disk import get_period_ecs_disk_cms








def get_ecs_stats(start: str, end: str) -> bool:
    """
    获取ECS监控统计数据（实例维度）,数据结构如下：
        {
        "<instance-id-001>":{
            "period_stats": {'total_cpu_cores': 4, 'total_mem_size': 8192, 'period_cpu_max_usage': 0.14, 'period_cpu_min_usage': 0.11, 'period_cpu_avg_usage': 0.12, 'period_mem_max_usage': 0.43, 'period_mem_min_usage': 0.41, 'period_mem_avg_usage': 0.42, 'total_cpu_max_used': 0.29, 'total_cpu_min_used': 0.22, 'total_cpu_avg_used': 0.24, 'total_mem_max_used': 1781.76, 'total_mem_min_used': 1781.76, 'total_mem_avg_used': 1781.76},
            "daily_data": {
                "2023-09-20": {'cpu_cores': '2', 'mem_size': '4096', 'cpu_max_usage': 0.14, 'cpu_min_usage': 0.11, 'cpu_avg_usage': 0.12, 'mem_max_usage': 0.43, 'mem_min_usage': 0.41, 'mem_avg_usage': 0.42, 'cpu_max_used': 0.29, 'cpu_min_used': 0.22, 'cpu_avg_used': 0.24, 'mem_max_used': 1781.76, 'mem_min_used': 1781.76, 'mem_avg_used': 1781.76},
                "2023-09-21": {'cpu_cores': '2', 'mem_size': '4096', 'cpu_max_usage': 0.14, 'cpu_min_usage': 0.11, 'cpu_avg_usage': 0.12, 'mem_max_usage': 0.43, 'mem_min_usage': 0.41, 'mem_avg_usage': 0.42, 'cpu_max_used': 0.29, 'cpu_min_used': 0.22, 'cpu_avg_used': 0.24, 'mem_max_used': 1781.76, 'mem_min_used': 1781.76, 'mem_avg_used': 1781.76},
            },
        },
    }
    :param start: 查询开始时间，例如: '2023-09-15'，查询结果包含开始时间
    :param end: 查询结束时间，例如: '2023-09-20'，查询结果不包含结束时间
    :return: 查询成功返回True，否则返回False
    """
    print("get_ecs_stats between: ", start, end)
    # 1 获取时间段内，所有ECS每日的信息(基础信息：每日cpu核数、内存大小)
    ecs_info_dt: dict = get_period_ecs_info(start, end)

    # 2 获取时间段内，所有ECS每日基础监控信息list
    ecs_cms_li: list = find_period_ecs_cms(start, end)

    # 2 获取时间段内，所有ECS每日Disk监控信息list
    period_ecs_disk_cms_dt = get_period_ecs_disk_cms(start, end)

    # 3 将cms监控数据与ECS每日信息dict合并
    ecs_stats_dt: dict = {}  # 因为cms监控数据的天数和 c_ecs_info 的天数可能不相同，故以cms为准，创建一个新的字典
    cms_dates_li: list = []  # 记录cms天数

    # 根据每天cms的使用率，计算当天的使用量，累计总使用量
    for i in ecs_cms_li:
        ins_id = i['instance_id']
        date = str(i['date'])
        cms_dates_li.append(date)

        # 实例名不存在
        if ins_id not in ecs_stats_dt.keys():
            ecs_stats_dt[ins_id] = {
                'days_cms': 0, 'period_cpu_cores': 0, 'period_mem_size': 0,
                'period_cpu_max_used': 0, 'period_cpu_min_used': 0, 'period_cpu_avg_used': 0,
                'period_mem_max_used': 0, 'period_mem_min_used': 0, 'period_mem_avg_used': 0,
                'cms_dates': []
            }
        # 为了避免同一天的数据重复出现，这里做了排除
        if date in ecs_stats_dt[ins_id]['cms_dates']:
            continue
        else:
            ecs_stats_dt[ins_id]['cms_dates'].append(date)

        # 获取当天的cpu核数、内存大小
        if date not in ecs_info_dt[ins_id]['daily_info'].keys():
            print(ins_id, "no such date in c_ecs_info:", date)
            continue
        cpu_cores_today = ecs_info_dt[ins_id]['daily_info'][date]['cpu_cores']
        mem_size_today = ecs_info_dt[ins_id]['daily_info'][date]['mem_size']
        # 计算当天的cpu、内存使用量
        cpu_max_used_today = float(i['cpu_max']) / 100 * cpu_cores_today
        cpu_min_used_today = float(i['cpu_min']) / 100 * cpu_cores_today
        cpu_avg_used_today = float(i['cpu_avg']) / 100 * cpu_cores_today
        mem_max_used_today = float(i['mem_max']) / 100 * mem_size_today
        mem_min_used_today = float(i['mem_min']) / 100 * mem_size_today
        mem_avg_used_today = float(i['mem_avg']) / 100 * mem_size_today

        # 阶段总量累加
        ecs_stats_dt[ins_id]['days_cms'] += 1
        ecs_stats_dt[ins_id]['period_cpu_cores'] += cpu_cores_today
        ecs_stats_dt[ins_id]['period_mem_size'] += mem_size_today
        ecs_stats_dt[ins_id]['period_cpu_max_used'] += cpu_max_used_today
        ecs_stats_dt[ins_id]['period_cpu_min_used'] += cpu_min_used_today
        ecs_stats_dt[ins_id]['period_cpu_avg_used'] += cpu_avg_used_today
        ecs_stats_dt[ins_id]['period_mem_max_used'] += mem_max_used_today
        ecs_stats_dt[ins_id]['period_mem_min_used'] += mem_min_used_today
        ecs_stats_dt[ins_id]['period_mem_avg_used'] += mem_avg_used_today

    print("cms_ecs dates: ", set(cms_dates_li))

    # 每天的数据统计完成后，进行阶段使用率的测算
    for ins_id in ecs_stats_dt.keys():
        # 阶段使用率 = 阶段使用量/阶段总量 * 100%
        ecs_stats_dt[ins_id]['period_cpu_max_usage'] = round(
            ecs_stats_dt[ins_id]['period_cpu_max_used'] / ecs_stats_dt[ins_id]['period_cpu_cores'], 4)
        ecs_stats_dt[ins_id]['period_cpu_min_usage'] = round(
            ecs_stats_dt[ins_id]['period_cpu_min_used'] / ecs_stats_dt[ins_id]['period_cpu_cores'], 4)
        ecs_stats_dt[ins_id]['period_cpu_avg_usage'] = round(
            ecs_stats_dt[ins_id]['period_cpu_avg_used'] / ecs_stats_dt[ins_id]['period_cpu_cores'], 4)
        ecs_stats_dt[ins_id]['period_mem_max_usage'] = round(
            ecs_stats_dt[ins_id]['period_mem_max_used'] / ecs_stats_dt[ins_id]['period_mem_size'], 4)
        ecs_stats_dt[ins_id]['period_mem_min_usage'] = round(
            ecs_stats_dt[ins_id]['period_mem_min_used'] / ecs_stats_dt[ins_id]['period_mem_size'], 4)
        ecs_stats_dt[ins_id]['period_mem_avg_usage'] = round(
            ecs_stats_dt[ins_id]['period_mem_avg_used'] / ecs_stats_dt[ins_id]['period_mem_size'], 4)
        # 规整小数点
        ecs_stats_dt[ins_id]['period_cpu_max_used'] = round(ecs_stats_dt[ins_id]['period_cpu_max_used'], 2)
        ecs_stats_dt[ins_id]['period_cpu_min_used'] = round(ecs_stats_dt[ins_id]['period_cpu_min_used'], 2)
        ecs_stats_dt[ins_id]['period_cpu_avg_used'] = round(ecs_stats_dt[ins_id]['period_cpu_avg_used'], 2)
        ecs_stats_dt[ins_id]['period_mem_max_used'] = round(ecs_stats_dt[ins_id]['period_mem_max_used'], 2)
        ecs_stats_dt[ins_id]['period_mem_min_used'] = round(ecs_stats_dt[ins_id]['period_mem_min_used'], 2)
        ecs_stats_dt[ins_id]['period_mem_avg_used'] = round(ecs_stats_dt[ins_id]['period_mem_avg_used'], 2)
        # 追加记录下最新的cpu核心数和内存容量
        ecs_stats_dt[ins_id]['cpu_cores_latest'] = ecs_info_dt[ins_id]['cpu_cores_latest']
        ecs_stats_dt[ins_id]['mem_size_latest'] = ecs_info_dt[ins_id]['mem_size_latest']
        # 记录c_ecs_info和cms_ecs天数的差值
        ecs_stats_dt[ins_id]['days_info'] = len(ecs_info_dt[ins_id]['info'])

        # 录入磁盘
        try:
            max_date = max(period_ecs_disk_cms_dt[ins_id]['daily_cms'].keys())
            ecs_stats_dt[ins_id]['disk_usage'] = period_ecs_disk_cms_dt[ins_id]['daily_cms'][max_date]['total_usage']
        except Exception as e:
            print(e)
            ecs_stats_dt[ins_id]['disk_usage'] = 0
    # 打印最终获取的ECS统计信息
    for k, v in ecs_stats_dt.items():
        print(k, v)

        # 将结果存入mysql中
        # cpu_use_percent = round(v['period_cpu_avg_usage'] * 100, 2)
        # mem_use_percent = round(v['period_mem_avg_usage'] * 100, 2)
        # disk_use_percent = round(v['disk_usage'], 2)
        #
        # sql = "UPDATE cmdb_cloud_resource SET cpu_use_percent='{}',mem_use_percent='{}',disk_use_percent='{}' WHERE instance_id='{}';".format(
        #     cpu_use_percent, mem_use_percent, disk_use_percent, k
        # )
        #
        # db_exec_one(sql)

    return True


@bp.route('/api/v1/cloud/generate_ecs_stats', methods=['POST'])
def generate_ecs_stats_api():
    """
    获取ECS监控统计数据（实例维度）API
    :param start: 查询开始时间，例如: '2023-09-15'，查询结果包含开始时间
    :param end: 查询结束时间，例如: '2023-09-20'，查询结果不包含结束时间
    :return: 返回Json Http响应
    """
    # today = (datetime.today()).strftime('%Y-%m-%d')
    # yesterday = (datetime.today() - timedelta(days=1)).strftime('%Y-%m-%d')
    start = request.args.get('start')  # "2023-09-21"
    end = request.args.get('end')  # "2023-09-24"
    success = get_ecs_stats(start, end)
    if not success:
        return jsonify({'code': 500})
    return jsonify({'code': 200})



