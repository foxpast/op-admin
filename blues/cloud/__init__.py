from flask import Blueprint

# 创建一个蓝图
bp = Blueprint("cloud_bp", __name__, template_folder="templates")

# 在__init__被执行的时候，把视图加载进来，让蓝图与应用程序知道有视图的存在
# 注意，必须先实例化bp，然后import其他。否则会造成循环import
# import下面之后，才会知道bp装饰器到底存在有哪些路由
from blues.cloud import ascm
from blues.cloud import ecs
from blues.cloud import rds
from blues.cloud import oss
from blues.cloud import slb
from blues.cloud import dds
from blues.cloud import cs
from blues.cloud import vpc
from blues.cloud import odps
from blues.cloud import cms_ecs
from blues.cloud import cms_ecs_disk
from blues.cloud import cms_rds
from blues.cloud import cms_oss
from blues.cloud import cms_mongo
from blues.cloud import cms_eip
from blues.cloud import stats_ecs_new
from blues.cloud import stats_odps
from blues.cloud import stats_resource_instance
from blues.cloud import stats_resource_daily
from blues.cloud import stats_resource_project
from blues.cloud import stats_resource_org

