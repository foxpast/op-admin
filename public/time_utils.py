import datetime
import time


def getTs():
    import time, calendar
    ts_float = time.time()
    ts_int = calendar.timegm(time.gmtime())
    # a = getNowTS()
    print(ts_float, ts_int)


def getUTCtime():
    # from datetime import datetime, timedelta
    now_time = datetime.datetime.now()
    utc_time = now_time - datetime.timedelta(hours=8)  # UTC只是比北京时间提前了8个小时
    utc_time = utc_time.strftime("%Y-%m-%dT%H:%M:%SZ")
    print(utc_time)


def getNowTS():
    now = time.strftime("%Y%m%d%H%M%S", time.localtime())
    return str(now)


def get_last_seven_days():
    now = datetime.datetime.now()
    before = now - datetime.timedelta(days=6)  # UTC只是比北京时间提前了8个小时
    now = now.strftime("%Y-%m-%d")
    before = before.strftime("%Y-%m-%d")
    print(before, now)
    return before, now


def seconds_to_days(num):
    days = int(num / (24 * 3600))
    hours = int(num % (24 * 3600) / 3600)
    minutes = int(num % 3600 / 60)
    res = "{}天{}时{}分".format(days, hours, minutes)
    print(res)
    return res


def get_delta_ts(t1, t2):
    """
    :param t1: 10位时间戳
    :param t2: 10位时间戳
    :return: 两个时间戳相减，返回秒数
    """
    res = int((t2 - t1))
    return res


def get_week(year=0, month=0, day=0):
    if year + month + day == 0:
        i = datetime.datetime.now()
    else:
        i = datetime.date(year, month, day)

    year, week, day = i.isocalendar()  # 年，第几周，周中第几天
    return year, week


def get_date_from_week_num(year, week):
    d = "{}-W{}".format(year, week)
    # print(d)
    r1 = datetime.datetime.strptime(d + '-1', "%Y-W%W-%w").date()  # 本周周一
    r7 = datetime.datetime.strptime(d + '-0', "%Y-W%W-%w").date()  # 本周周日
    text = "{} ~ {}".format(r1, r7)
    # print(text)
    return text


def get_range_ts(time_range):
    """
    :return: 获取精确到秒时间戳，10位
    """
    tr_dict = {
        'hour': 1,
        'day': 24,
        'week': 24 * 7
    }
    end = datetime.datetime.now()
    end = int(end.timestamp())
    start = datetime.datetime.now() - datetime.timedelta(hours=tr_dict[time_range])
    start = int(start.timestamp())
    return start, end


def get_last_2days_0hour():
    now = datetime.datetime.now()
    before = now - datetime.timedelta(days=1)  # UTC只是比北京时间提前了8个小时
    now = now.strftime("%Y-%m-%d 00:00:00")
    before = before.strftime("%Y-%m-%d 00:00:00")
    print(before, now)
    return before, now


if __name__ == '__main__':
    get_last_2days_0hour()