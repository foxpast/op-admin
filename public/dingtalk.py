# coding=utf-8
import json
import requests

from config import Config

conf = Config()

headers = {'Content-Type': 'application/json;charset=utf-8'}


def job_create_msg(content, phone):
    json_text = {
        "msgtype": "text",
        "text": {
            "content": content
        },
        "at": {
            "atMobiles": [
                phone
            ],
            "isAtAll": False
        }
    }
    resp = requests.post(conf.WEBHOOK, json.dumps(json_text), headers=headers)
    print(resp.text)
    # if WEBHOOK2 != WEBHOOK:
    #     resp2 = requests.post(WEBHOOK2, json.dumps(json_text), headers=headers).content
    #     print(resp2)


def job_succeed_msg(content, at_all=True):
    json_text = {
        "msgtype": "text",
        "text": {
            "content": content
        },
        "at": {
            "atMobiles": [
            ],
            "isAtAll": at_all
        }
    }
    resp = requests.post(conf.WEBHOOK, json.dumps(json_text), headers=headers)
    print(resp.text)

