import datetime



def json2yaml(jstr):
    import yaml, json
    # json数据转换为python数据，使用loads函数
    data = json.loads(jstr)
    ystr = yaml.dump(data, allow_unicode=True)
    # print(ystr)
    return ystr


def str_presenter(dumper, data):
    # check for multiline strings
    if len(data.splitlines()) == 1 and data[-1] == '\n':
        return dumper.represent_scalar('tag:yaml.org,2002:str', data, style='>')
    if len(data.splitlines()) > 1:
        return dumper.represent_scalar('tag:yaml.org,2002:str', data, style='|')
    return dumper.represent_scalar('tag:yaml.org,2002:str', data.strip())


def json2yaml(jstr):
    import yaml, json
    # json数据转换为python数据，使用loads函数
    data = json.loads(jstr)
    yaml.add_representer(str, str_presenter)
    ystr = yaml.dump(data, allow_unicode=True, width=100000)
    # print(ystr)
    return ystr










if __name__ == '__main__':
    # b = '{"ip": "172.16.0.226"}'
    # json2yaml(b)
    # print(get_second_ts('hour'))
    # print(get_week())
    # get_date_from_week_num(2022, 33)
    # get_last_seven_days()
    # seconds_to_days(812338)
    import time, calendar

    a = time.time()
    ts = calendar.timegm(time.gmtime())
    # a = getNowTS()
    print(a, ts)