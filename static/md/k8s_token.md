# K8s集群参数获取方式
## 1. Api Server

在集群master节点执行如下操作，获取Kubernetes master的地址
```shell
$ kubectl cluster-info
Kubernetes master is running at https://172.18.35.11:6443
CoreDNS is running at https://172.18.35.11:6443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy
```


## 2. Token
首先创建文件 k8s-token.yaml

```
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: k8s-authorize
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: k8s-authorize
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: k8s-authorize
  namespace: kube-system
```

应用该yaml文件，并获取token

```
$ kubectl apply -f k8s-token.yaml
$ kubectl describe secrets -n kube-system   k8s-authorize
Name:         k8s-authorize-token-7ksbc
Namespace:    kube-system
Labels:       <none>
Annotations:  kubernetes.io/service-account.name: k8s-authorize
              kubernetes.io/service-account.uid: 2aaf09f9-de33-4ecf-ae47-2e12ee7b9770

Type:  kubernetes.io/service-account-token

Data
====
token:      <Token 内容>
ca.crt:     1025 bytes
namespace:  11 bytes
```

拷贝打印出的<Token 内容>

## 3. Prom
Prometheus服务对外暴露的地址，例如``http://119.45.8.107:30002``，可使用nginx做反向代理的地址