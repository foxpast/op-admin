node{
    stage('Git Checkout Source Code'){
        echo '下载代码...'
        checkout([$class: 'GitSCM', branches: [[name: '$Branches']], doGenerateSubmoduleConfigurations: false, extensions: [], submoduleCfg: [], userRemoteConfigs: [[credentialsId: 'd65cafd5-3cff-4b2b-9fe2-643c4218034e', url: '__CODE_REPO__']]])
    }

    stage('Git Checkout Docker&K8s Config'){
        echo '下载打包部署配置...'
        checkout([$class: 'GitSCM', branches: [[name: 'origin/main']], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'k8s-config']], submoduleCfg: [], userRemoteConfigs: [[credentialsId: 'd65cafd5-3cff-4b2b-9fe2-643c4218034e', url: '__CONF_REPO__']]])
        sh'''
            rm -rf k8s-config.yaml Dockerfile Jenkinsfile
            cp ./__CLUSTER__/__APP_NAME__/* ./
        '''
    }

    stage('Build & Scan'){}


    stage('Build & Push Image'){
        echo '打包推送Docker镜像...'
        withCredentials([usernamePassword(credentialsId: 'HARBOR_PASS', passwordVariable: 'password', usernameVariable: 'username')]) {
            sh '''
                Registry=harbor.yunyang.com.cn
                Image=__IMAGE__
                docker login -u ${username} -p ${password} $Registry
                docker buildx build --platform linux/amd64 -t $Image:${version} -f Dockerfile --push .
            '''
        }
    }

    stage('Deploy to K8S'){}

    stage ('Post-Build') {
        echo "移除本地构建的镜像"
        sh '''
            docker image rm -f `docker image ls | grep __APP_NAME__ | awk '{print $3}'`
        '''
        script {
            currentBuild.description = "${version}"
        }
    }
}